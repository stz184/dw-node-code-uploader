var path      = require('path');
var fs        = require('fs');
var Server    = require('./server');
var Resource  = require('./resource');
var watch     = require('node-watch');

module.exports = function(configFilePath, cartridgesPath) {
    var config  = JSON.parse(fs.readFileSync(configFilePath));

    var host        = config.host;
    var version     = config.version;
    var username    = config.username;
    var password    = config.password;

    console.log("Watching...");

    watch(cartridgesPath, { recursive: true }, function(evt, fileName) {
        var basename    = path.basename(fileName);
        var isDotFile   = basename.search(/^\./) > -1;
        
        if (!isDotFile) {
            var resource    = new Resource(fileName);

            if (resource.isInCartridge() && resource.isFile()) {
                console.log("Attempting server upload of " + resource.codePath + " to " + host);
                var server = new Server(host, username, password, version);
                server.upload(
                    resource,
                    function() {
                        console.info("Upload sucessfully finished.", fileName);
                    },
                    function(e) {
                        console.error("Upload Failed " + e);
                    }
                );
            } 
        }
    });
}